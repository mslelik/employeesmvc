﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeesMVC.Models
{
    public static class EmployeesDataProvider
    {
        const string EmployeesDBEntitiesKey = "EmployeesDBEntities";

        public static EmployeesDBEntities DB
        {
            get
            {
                if (HttpContext.Current.Items[EmployeesDBEntitiesKey] == null)
                    HttpContext.Current.Items[EmployeesDBEntitiesKey] = new EmployeesDBEntities();
                return (EmployeesDBEntities)HttpContext.Current.Items[EmployeesDBEntitiesKey];
            }
        }

        public static IEnumerable<Department> GetDepartments()
        {
            return DB.Departments.ToList();
        }

        public static IEnumerable<string> GetGenders()
        {
            return null;
        }
    }
}