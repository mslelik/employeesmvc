﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EmployeesMVC.Models;
using DevExpress.Web.Mvc;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace EmployeesMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        EmployeesMVC.Models.EmployeesDBEntities db1 = new EmployeesMVC.Models.EmployeesDBEntities();

        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            var model = db1.Employees;
            return PartialView("_GridViewPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialAddNew([ModelBinder(typeof(DevExpressEditorsBinder))] EmployeesMVC.Models.Employee item)
        {
            var model = db1.Employees;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db1.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate([ModelBinder(typeof(DevExpressEditorsBinder))] EmployeesMVC.Models.Employee item)
        {
            var model = db1.Employees;

            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.EmployeeId == item.EmployeeId);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db1.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialDelete(System.Int32 EmployeeId)
        {
            var model = db1.Employees;
            if (EmployeeId >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.EmployeeId == EmployeeId);
                    if (item != null)
                        model.Remove(item);
                    db1.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridViewPartial", model.ToList());
        }

        public ActionResult ImageUpload()
        {
            UploadControlExtension.GetUploadedFiles("uploadControl", UploadControlHelper.ValidationSettings, UploadControlHelper.uploadControl_FileUploadComplete);
            return null;
        }
    }

    public class UploadControlHelper
    {
        /// <summary>
        /// Validation settings for files to upload
        /// </summary>
        public static readonly ValidationSettings ValidationSettings = new ValidationSettings
        {
            AllowedFileExtensions = new string[] { ".jpg", ".jpeg", ".jpe" },
            MaxFileSize = 4000000
        };

        public static void uploadControl_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.UploadedFile.IsValid)
            {
                string resultFilePath = "~/Content/ProfileImages/" + 
                    string.Format("profilePic{0}{1}", Convert.ToString(HttpContext.Current.Session["EmployeeId"]), Path.GetExtension(e.UploadedFile.FileName));
                e.UploadedFile.SaveAs(HttpContext.Current.Request.MapPath(resultFilePath));
                HttpContext.Current.Session["imagePath"] = resultFilePath;
                System.Web.UI.IUrlResolutionService urlResolver = sender as System.Web.UI.IUrlResolutionService;
                if (urlResolver != null)
                    e.CallbackData = resultFilePath;
            }
        }
    }
}